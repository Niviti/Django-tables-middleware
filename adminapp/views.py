from django.shortcuts import render

# Create your views here.
from django.views.generic.list import ListView
from dziekanat.models import Student
from .tables import StudentTable
from django_tables2 import RequestConfig

class Dashboradpage(ListView):

    def get(self, request, *args, **kwargs):

       table = StudentTable(Student.objects.all())

       table.paginate(page=request.GET.get('page', 1), per_page=5)

       ##RequestConfig(request).configure(table)
       return render(request, 'dashboard.html', {'table': table})