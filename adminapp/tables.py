


import django_tables2 as tables
from django_tables2 import TableBase, TemplateColumn, Column

from dziekanat.models import Student




class StudentTable(tables.Table):

    testowe = TemplateColumn(' <a href="/test"> test </a>  ')
    name = tables.Column()
    directory = tables.Column()
    year = tables.Column()

    class Meta:
        attrs = {'class': 'mytable'}
        model = Student
        fields = ('name', 'directory', 'year')
        sequence = (fields)
        template_name = 'django_tables2/bootstrap.html'