
from django.db import models


class Student(models.Model):
    name =models.CharField(max_length=30, null=True, blank=True)
    album= models.IntegerField(null=True, blank=True)
    directory= models.CharField(max_length=30, null=True, blank=True)
    year= models.IntegerField(null=True, blank=True)
    status= models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return self.name
