from django import template

register = template.Library()


@register.inclusion_tag('facebook_sidebar.html', takes_context=False)
def  facebook_sidebar():
    return {
        'template_name' : 'facebook_sidebar.html'
    }