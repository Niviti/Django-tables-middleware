# Generated by Django 2.0.5 on 2018-05-19 22:16

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=30, null=True)),
                ('album', models.IntegerField(blank=True, null=True)),
                ('directory', models.CharField(blank=True, max_length=30, null=True)),
                ('year', models.IntegerField(blank=True, null=True)),
                ('status', models.CharField(blank=True, max_length=30, null=True)),
            ],
        ),
    ]
